# 小米路由器 mini

主流雙頻 AC 智慧路由器，性價比之王

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white "本專案使用 pre-commit 檢查專案中的潛在問題")](https://github.com/pre-commit/pre-commit) [![REUSE 規範遵從狀態標章](https://api.reuse.software/badge/github.com/libre-knowledge/subject-template "本專案遵從 REUSE 規範降低軟體授權合規成本")](https://api.reuse.software/info/gitlab.com/libre-knowledge/miwifi-mini)

## 參考資料

* [MT7620 Datasheet E4](https://edit.wpgdadawant.com/uploads/news_file/blog/2020/2404/tech_files/blog_2404_suggest_other_file.pdf)  
  Datasheet of the SoC chipset
* [MT7620 Programming Guide](https://download.villagetelco.org/hardware/MT7620/MT7620-Ralink%281%29.pdf)  
  Programming guide of the SoC chipset

---

本主題為[自由知識協作平台](https://libre-knowledge.github.io/)的一部分，除部份特別標註之經合理使用(fair use)原則使用的內容外允許公眾於授權範圍內自由使用

如有任何問題，歡迎於本主題的[議題追蹤系統](https://gitlab.com/libre-knowledge/miwifi-mini/-/issues)創建新議題反饋
